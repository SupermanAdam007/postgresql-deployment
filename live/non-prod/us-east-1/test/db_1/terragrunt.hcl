locals {
  # Automatically load environment-level variables
  environment_vars = read_terragrunt_config(find_in_parent_folders("env.hcl"))
}

# Terragrunt will copy the Terraform configurations specified by the source parameter, along with any files in the
# working directory, into a temporary folder, and execute your Terraform commands in that folder.
terraform {
  source = "git::git@gitlab.com:SupermanAdam007/postgresql-manager.git//postgres-manager?ref=v0.1.1"
}

# Include all settings from the root terragrunt.hcl file
include {
  path = find_in_parent_folders()
}

# These are the variables we have to pass in to use the module specified in the terragrunt configuration above
inputs = {
  port = 5443
  number_of_databases = 3
  # Taken from env variable connection_password_non_prod_us_east_1_test_db_1
  connection_password = get_env("connection_password_non_prod_us_east_1_test_db_1")
}
