locals {
  account_name   = "non-prod"
  aws_account_id = "dummy_aws_account_id"
  aws_profile    = "non-prod"
}