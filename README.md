# PostgreSQL Deployment
Creates Terragrunt ecosystem for managing PostgreSQL databases with Terraform module for PostgreSQL management: 
https://gitlab.com/SupermanAdam007/postgresql-manager

## Features
1. Actual PostgreSQL management Terraform code is DRY, it's never repeated
2. Terraform state is DRY, one can manage multiple remote states for multiple AWS accounts or environments easily
3. One can deploy whole environment (or some subset) at once
4. Adding a new instance is about adding one file (terragrunt.hcl)
5. On every instance there can be different version of Terraform package (i.e. CI/CD - deploy it on dev -> test -> prod), versioned with tags in repository https://gitlab.com/SupermanAdam007/postgresql-manager

## How to use stack
### 0. Preparation
#### Conda environment
Create and use conda environment (this helps to specify Terraform and Terragrunt versions to prevent common issues).
```shell
./scripts/create_conda_env.sh
conda activate .conda-venv/
```

#### Exporting master passwords of databases
For deployment process is currently (they should be i.e. in AWS Secret Manager, GitLab CI, ...) needed exporting of credentials each PostgreSQL instance.
```shell
export connection_password_non_prod_us_east_1_dev_db_1=r6y5456g10bxc0b
export connection_password_non_prod_us_east_1_dev_db_2=yr64k65k15s11uk
export connection_password_non_prod_us_east_1_test_db_1=s6d5465s1dv13dv
```

### 1. Spin up PostgreSQL instances
```shell
docker-compose up -d
```

### 2. Deployment
Each deployment is in the correct structure for AWS (different accounts/regions/deployments).
#### Deployment for single database
```shell
cd live/non-prod/us-east-1/dev/db_1
terragrunt apply
```

#### Deployment for dev env (currently 2 instances)
```shell
cd live/non-prod/us-east-1/dev
terragrunt run-all apply
```

#### Deployment for all envs in region at once
```shell
cd live/non-prod/us-east-1
terragrunt run-all apply
```

### 3. Check PostgreSQL instance
For each generated user the password is generated. To get them run:
```shell
./scripts/find_tfstate_files.sh

OUTPUT:
./live/non-prod/us-east-1/dev/db_2/.terragrunt-cache/.../.../postgres-manager/terraform.tfstate
./live/non-prod/us-east-1/dev/db_1/.terragrunt-cache/.../.../postgres-manager/terraform.tfstate
./live/non-prod/us-east-1/test/db_1/.terragrunt-cache/.../.../postgres-manager/terraform.tfstate

./scripts/get_credentials_from_tfstate.sh ./live/non-prod/us-east-1/dev/db_2/.terragrunt-cache/.../.../postgres-manager/terraform.tfstate
OUTPUT:
[{"users":{"username":"all_dbs_read_only","password":"..."}}]
[{"users":{"username":"tf_db_owners_role_0","password":"..."}}]
[{"users":{"username":"tf_db_owners_role_1","password":"..."}}]
[{"users":{"username":"tf_db_owners_role_2","password":"..."}}]

./scripts/get_credentials_from_tfstate.sh ./live/non-prod/us-east-1/dev/db_1/.terragrunt-cache/.../.../postgres-manager/terraform.tfstate
OUTPUT:
[{"users":{"username":"all_dbs_read_only","password":"..."}}]
[{"users":{"username":"tf_db_owners_role_0","password":"..."}}]
[{"users":{"username":"tf_db_owners_role_1","password":"..."}}]
[{"users":{"username":"tf_db_owners_role_2","password":"..."}}]

./scripts/get_credentials_from_tfstate.sh ./live/non-prod/us-east-1/test/db_1/.terragrunt-cache/.../.../postgres-manager/terraform.tfstate
OUTPUT:
[{"users":{"username":"all_dbs_read_only","password":"..."}}]
[{"users":{"username":"tf_db_owners_role_0","password":"..."}}]
[{"users":{"username":"tf_db_owners_role_1","password":"..."}}]
[{"users":{"username":"tf_db_owners_role_2","password":"..."}}]
```
In real world, these passwords should be fetched or uploaded in AWS Secret Manager.

With those one can try to login to some database. I'm personally using DBeaver.
