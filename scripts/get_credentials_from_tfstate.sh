#!/usr/bin/env bash

TFSTATE_FILE=$1

if [ -z "$TFSTATE_FILE" ]
then
  echo "Please specify terraform.tfstate file"
  exit 1
fi

cat $TFSTATE_FILE | jq -c '.resources[].instances[] | select(.attributes.password != null) | [{"users": {"username": .attributes.name, "password": .attributes.password}}]'
